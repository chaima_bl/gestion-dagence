
<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">
			<li class="sidebar-search">
				<div class="input-group custom-search-form">
					<input type="text" class="form-control" placeholder="Recherche...">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">
							<i class="fa fa-search"></i>
						</button>
					</span>
				</div> <!-- /input-group -->
			</li>
			<c:url value="/home/" var="home"/>
			<li><a href="${home}"><i class="fa fa-dashboard fa-fw"></i>
					<fmt:message key="common.dashboard"></fmt:message></a></li>
					
					<c:url value="/passagers/" var="passagers"/>
					<li><a href="${ passagers}"><i class="fa fa fa-users fa-fw"></i> 
			<fmt:message key="common.passagers"></fmt:message><span class="fa arrow"></span></a>
				
				<ul class="nav nav-second-level">
				    
					<li><a href="${passagers }"><fmt:message key="common.passagers.liste"></fmt:message></a></li>
					
					<li><a href="${passagers }"><fmt:message key="common.passagers.update"></fmt:message></a></li>
					
				</ul> <!-- /.nav-second-level --></li>
			<c:url value="/reservation/" var="reservation"/>		
			<li><a href="${reservation }"><i class="fa fa fa-calendar fa-fw"></i> 
			<fmt:message key="common.reservation"></fmt:message><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li><a href="${reservation }"><fmt:message key="common.reservation.liste"></fmt:message></a></li>
					<li><a href="${reservation }"><fmt:message key="common.reservation.update"></fmt:message></a></li>
					
				</ul> <!-- /.nav-second-level --></li>
			<li><a href="#"><i class="fa fa fa-plane fa-fw"></i> 
			<fmt:message key="common.voyage"></fmt:message><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li><a href="flot.html"><fmt:message key="common.voyage.liste"></fmt:message></a></li>
					<li><a href="morris.html"><fmt:message key="common.voyage.update"></fmt:message></a></li>
				</ul> <!-- /.nav-second-level --></li>
					<li><a href="#"><i class="fa fa-sitemap fa-fw"></i>
					<fmt:message key="common.tours"></fmt:message><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li><a href="#">Second Level Item</a></li>
					<li><a href="#">Second Level Item</a></li>
					<li><a href="#">Third Level <span class="fa arrow"></span></a>
						<ul class="nav nav-third-level">
							<li><a href="#">Third Level Item</a></li>
							<li><a href="#">Third Level Item</a></li>
							<li><a href="#">Third Level Item</a></li>
							<li><a href="#">Third Level Item</a></li>
						</ul> <!-- /.nav-third-level --></li>
				</ul> <!-- /.nav-second-level --></li>
			<li><a href="tables.html"><i class="fa fa fa-cc-visa fa-fw"></i>
					<fmt:message key="common.paiement"></fmt:message>s</a></li>
			
<!-- 			<li><a href="#"><i class="fa fa-wrench fa-fw"></i>  -->
<!-- 					<fmt:message key="common.parametrage"></fmt:message><span class="fa arrow"></span></a> -->
<!-- 				<ul class="nav nav-second-level"> -->
<!-- 					<li><a href="panels-wells.html">Panels and Wells</a></li> -->
<!-- 					<li><a href="buttons.html">Buttons</a></li> -->
<!-- 					<li><a href="notifications.html">Notifications</a></li> -->
<!-- 					<li><a href="typography.html">Typography</a></li> -->
<!-- 					<li><a href="icons.html"> Icons</a></li> -->
<!-- 					<li><a href="grid.html">Grid</a></li> -->
<!-- 				</ul> /.nav-second-level</li> -->
		
			<li><a href="#"><i
					class="fa fa-wrench fa-fw"></i> <fmt:message key="common.outils"></fmt:message><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li><a class="active" href="blank.html">Blank Page</a></li>
					<li><a href="login.html">Login Page</a></li>
				</ul><!-- /.nav-second-level --></li>
		</ul>
	</div>
	<!-- /.sidebar-collapse -->
</div>