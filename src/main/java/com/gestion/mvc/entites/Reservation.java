package com.gestion.mvc.entites;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@SequenceGenerator(name = "seq_resa", sequenceName = "seq_resa", initialValue = 1, allocationSize = 1)
@Table
public class Reservation implements Serializable {

	@Id
	@GeneratedValue(generator = "seq_resa")
	private Long idRes;

	@Column
	private String numRes;

	@Column
	private Date date;

	@ManyToOne
	@JoinColumn(name = "idVol")
	private Vol vol;

//	@ManyToOne
//	@JoinColumn(name = "idP")
//	private Passagers p;

	public Reservation() {
	}

	public Reservation(Vol v, Date date, String numRes) {
		this.date = date;
		this.numRes = numRes;
		this.vol = v;
	}

	public Long getId() {
		return idRes;
	}

	public void setNumReservation(String numRes) {
		this.numRes = numRes;
	}

	public String getNumRes() {
		return numRes;
	}
}
