package com.gestion.mvc.services;

import java.util.List;

import com.gestion.mvc.entites.Vol;

public interface IVolService {
	
	public Vol save(Vol entity);

	public Vol Update(Vol entity);

	public List<Vol> selectAll();

	public List<Vol> selectAll(String sortField, String sort);

	public Vol getById(Long id);

	public void remove(Long id);

	public Vol findOne(String paramName, Object paramValue);

	public Vol findOne(String[] paramName, Object[] paramValue);

	public int findCountBy(String paramName, String paramValue);

}
