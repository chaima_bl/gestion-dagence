package com.gestion.mvc.services.Impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gestion.mvc.dao.IVolDao;
import com.gestion.mvc.entites.Vol;
import com.gestion.mvc.services.IVolService;

@Transactional
public class VolServiceImpl implements IVolService {

	private IVolDao dao;

	public void setDao(IVolDao dao) {
		this.dao = dao;
	}

	@Override
	public Vol save(Vol entity) {
		return dao.save(entity);
	}

	@Override
	public Vol Update(Vol entity) {
		return dao.Update(entity);
	}

	@Override
	public List<Vol> selectAll() {
		return dao.selectAll();
	}

	@Override
	public List<Vol> selectAll(String sortField, String sort) {
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Vol getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public Vol findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Vol findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		return dao.findCountBy(paramName, paramValue);
	}

}
