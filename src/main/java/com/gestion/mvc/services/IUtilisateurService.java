package com.gestion.mvc.services;

import java.util.List;

import com.gestion.mvc.entites.Utilisateur;

public interface IUtilisateurService {
	
	public Utilisateur save(Utilisateur entity);

	public Utilisateur Update(Utilisateur entity);

	public List<Utilisateur> selectAll();

	public List<Utilisateur> selectAll(String sortField, String sort);

	public Utilisateur getById(Long id);

	public void remove(Long id);

	public Utilisateur findOne(String paramName, Object paramValue);

	public Utilisateur findOne(String[] paramName, Object[] paramValue);

	public int findCountBy(String paramName, String paramValue);

}
