package com.gestion.mvc.dao;

import com.gestion.mvc.entites.Utilisateur;

public interface IUtilisateurDao extends IGenericDao<Utilisateur> {

}
