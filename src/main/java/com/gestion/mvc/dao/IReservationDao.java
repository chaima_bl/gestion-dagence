package com.gestion.mvc.dao;

import com.gestion.mvc.entites.Reservation;

public interface IReservationDao extends IGenericDao<Reservation> {

}
